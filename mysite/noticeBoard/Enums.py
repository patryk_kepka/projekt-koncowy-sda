from enum import Enum
from django.db import models


class Currency(models.TextChoices):
    PLN = 'Złoty', 'PLN'
    USD = 'Dolar', 'USD'
    EUR = 'Euro', 'EUR'
    GBP = 'Funt', 'GBP'


class RoomsNumber(models.TextChoices):
    studio_apartment = 'kawalerka'
    two_rooms = 'dwa pokoje'
    three_rooms = 'trzy pokoje'
    four_rooms = 'cztery pokoje'
    over = 'więcej pokoi'


class Market(models.TextChoices):
    primary = 'rynek pierowtny'
    afetermarket = 'rynek wtórny'


class TypeOfBuilding(models.TextChoices):
    block = 'blok'
    tenement_house = 'kamienica'
    a_detached_house = 'dom wolnostojący'
    terraced_house = 'szeregowiec'
    apartment_building = 'apartamentowiec'
    loft = 'loft'


class BuildingMaterial(models.TextChoices):
    brick = 'cegła'
    wood = 'drewno'
    airbrick = 'pustak'
    expanded_clay = 'keramzyt'
    large_slab = 'wielka płyta'
    concrete = 'beton'
    silicate = 'silikat'
    cellular_concrete = 'beton komórkowy'
    reinforced_concrete = 'żelbet'
    other = 'inne'


class Windows(models.TextChoices):
    plastic = 'plastikowe'
    wooden = 'drewniane'
    aluminum = 'aluminiowe'


class Heating(models.TextChoices):
    district = 'miejskie'
    gas = 'gazowe'
    tiled_stoves = 'piece kaflowe'
    electric = 'elektryczne'
    boiler = 'kotłownia'
    other = 'inne'


class FinishCondition(models.TextChoices):
    to_live = 'do zamieszkania'
    to_finish = 'do wykończenia'
    for_renovation = 'do remontu'


class FormOfTheProperty(models.TextChoices):
    cooperative_ownership = 'spółdzielcze własnościowe'
    cooperative_ownership_with_a_land_and_mortgage_register = 'spółdzielcze własnościowe z KW'
    full_ownership = 'pełna własność'
    share = 'udział'
