from django.views import View
from django.views.generic import FormView, ListView, DetailView
from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from noticeBoard.forms import AdForm, SearchAdForm
from noticeBoard.models import AdModel
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.shortcuts import render
from .forms import NewUserModelForm


class NewUser(CreateView):
    template_name = "new_user.html"
    success_url = reverse_lazy("good_new_user")
    form_class = NewUserModelForm


# do usunięcia po rozwinięciu
def good_job(request):
    return render(
        request,
        template_name='good_job.html'
    )


def good_bye(request):
    return render(
        request,
        template_name='good_bye.html'
    )


def good_new_user(request):
    return render(
        request,
        template_name='good_new_user.html'
    )


class AdView(View):
    def get(self, request):
        form = AdForm()
        return render(
            request,
            template_name="advertising.html",
            context={"form": form}
        )

    def post(self, request):
        form = AdForm(request.POST)
        if form.is_valid():
            AdModel.objects.create(
                title=form.cleaned_data["title"],
                price=form.cleaned_data["price"],
                currency_for_price=form.cleaned_data["currency_for_price"],
                area=form.cleaned_data["area"],
                rooms_number=form.cleaned_data["rooms_number"],
                market=form.cleaned_data["market"],
                advertiser_type=form.cleaned_data["advertiser_type"],
                # image=form.cleaned_data["image"],
                movie=form.cleaned_data["movie"],
                virtual_walk=form.cleaned_data["virtual_walk"],
                development_plan=form.cleaned_data["development_plan"],
                city=form.cleaned_data["city"],
                district=form.cleaned_data["district"],
                street=form.cleaned_data["street"],
                description=form.cleaned_data["description"],
                type_of_building=form.cleaned_data["type_of_building"],
                floor=form.cleaned_data["floor"],
                number_of_floors=form.cleaned_data["number_of_floors"],
                building_material=form.cleaned_data["building_material"],
                windows=form.cleaned_data["windows"],
                heating=form.cleaned_data["heating"],
                year_of_construction=form.cleaned_data["year_of_construction"],
                finish_condition=form.cleaned_data["finish_condition"],
                rent=form.cleaned_data["rent"],
                currency_for_rent=form.cleaned_data["currency_for_rent"],
                form_of_the_property=form.cleaned_data["form_of_the_property"],
                available_since=form.cleaned_data["available_since"],
                internet=form.cleaned_data["internet"],
                cable_tv=form.cleaned_data["cable_tv"],
                phone=form.cleaned_data["phone"],
                anti_burglary_blinds=form.cleaned_data["anti_burglary_blinds"],
                security_doors_windows=form.cleaned_data["security_doors_windows"],
                intercom_videophone=form.cleaned_data["intercom_videophone"],
                monitoring_security=form.cleaned_data["monitoring_security"],
                alarm_system=form.cleaned_data["alarm_system"],
                closed_area=form.cleaned_data["closed_area"],
                furniture=form.cleaned_data["furniture"],
                washing_machine=form.cleaned_data["washing_machine"],
                dishwasher=form.cleaned_data["dishwasher"],
                fridge=form.cleaned_data["fridge"],
                stove=form.cleaned_data["stove"],
                oven=form.cleaned_data["oven"],
                tv=form.cleaned_data["tv"],
                balcony=form.cleaned_data["balcony"],
                utility_room=form.cleaned_data["utility_room"],
                garage_parking_space=form.cleaned_data["garage_parking_space"],
                cellar=form.cleaned_data["cellar"],
                garden=form.cleaned_data["garden"],
                terrace=form.cleaned_data["terrace"],
                elevator=form.cleaned_data["elevator"],
                two_level=form.cleaned_data["two_level"],
                separate_kitchen=form.cleaned_data["separate_kitchen"],
                air_conditioning=form.cleaned_data["air_conditioning"],
            )
            return HttpResponseRedirect(reverse("noticeBoard:my-advertising"))
        return render(
            request,
            template_name="advertising.html",
            context={"form": form}
        )


class SearchAdFormView(FormView):
    template_name = 'search.html'
    form_class = SearchAdForm
    success_url = reverse_lazy('noticeBoard:my-advertising')

    def form_valid(self, form):
        result = super().form_valid(form)
        AdModel.objects.filter(
            price=form.cleaned_data["price"],
            area=form.cleaned_data["area"],
            rooms_number=form.cleaned_data["rooms_number"],
            market=form.cleaned_data["market"],
            advertiser_type=form.cleaned_data["advertiser_type"],
            city=form.cleaned_data["city"],
            district=form.cleaned_data["district"],
            street=form.cleaned_data["street"],
            type_of_building=form.cleaned_data["type_of_building"],
            floor=form.cleaned_data["floor"],
            number_of_floors=form.cleaned_data["number_of_floors"],
            building_material=form.cleaned_data["building_material"],
            windows=form.cleaned_data["windows"],
            heating=form.cleaned_data["heating"],
            year_of_construction=form.cleaned_data["year_of_construction"],
            finish_condition=form.cleaned_data["finish_condition"],
            rent=form.cleaned_data["rent"],
            form_of_the_property=form.cleaned_data["form_of_the_property"],
            available_since=form.cleaned_data["available_since"],
            )
        return HttpResponseRedirect(reverse("noticeBoard:my-advertising"))
    # listView - renderowanie strony z listą ogłoszeń

    def form_invalid(self, form):
        return super().form_invalid(form)


class AdDetailView(DetailView):
    model = AdModel
    template_name = 'ad_detail.html'


class AdListView(ListView):
    template_name = 'ad_list.html'
    model = AdModel
