from django import forms
from noticeBoard.models import UserProfile
from django.contrib.auth.mixins import UserPassesTestMixin


ADVERTISER_TYPE = [
    ('property owner', 'Property Owner'),
    ('broker', 'Broker'),
]


class SearchAdForm(forms.Form):
    price = forms.DecimalField(label="Cena", decimal_places=2, max_digits=50)
    area = forms.DecimalField(label='Powierzchnia', decimal_places=2, max_digits=50)
    rooms_number = forms.ChoiceField(choices=RoomsNumber.choices, label='Liczba pokoi')
    market = forms.ChoiceField(choices=Market.choices, label='Rynek')
    advertiser_type = forms.CharField(label='Typ ogłoszenia', widget=forms.RadioSelect(choices=ADVERTISER_TYPE))
    city = forms.CharField(label='Miejscowość', max_length=25)
    district = forms.CharField(label='Dzielnica', max_length=25)
    street = forms.CharField(label='ulica', max_length=50)
    type_of_building = forms.ChoiceField(choices=TypeOfBuilding.choices, label='Rodzaj zabudowy')
    floor = forms.IntegerField(label='Piętro')
    number_of_floors = forms.IntegerField(label='Liczba pięter')
    building_material = forms.ChoiceField(choices=BuildingMaterial.choices, label='Materiał budynku')
    windows = forms.ChoiceField(choices=Windows.choices, label='Okna')
    heating = forms.ChoiceField(choices=Heating.choices, label='Ogrzewanie')
    year_of_construction = forms.IntegerField(label='Rok budowy')
    finish_condition = forms.ChoiceField(choices=FinishCondition.choices, label='Stan wykończenia')
    rent = forms.IntegerField(label='Czynsz')
    form_of_the_property = forms.ChoiceField(choices=FormOfTheProperty.choices, label='Forma własności')
    available_since = forms.DateTimeField(label='Dostępne od')


class AdForm(forms.Form):
    title = forms.CharField(label='Tytuł ogłoszenia', max_length=50)
    price = forms.DecimalField(label="Cena", decimal_places=2, max_digits=50)
    currency_for_price = forms.ChoiceField(choices=Currency.choices, label='')
    area = forms.DecimalField(label='Powierzchnia', decimal_places=2, max_digits=50)
    rooms_number = forms.ChoiceField(choices=RoomsNumber.choices, label='Liczba pokoi')
    market = forms.ChoiceField(choices=Market.choices, label='Rynek')
    advertiser_type = forms.CharField(label='Typ ogłoszenia', widget=forms.RadioSelect(choices=ADVERTISER_TYPE))
    # image = forms.ImageField(label='zdjęcia')
    movie = forms.URLField(label='Film', max_length=100, required=False)
    virtual_walk = forms.URLField(label='Wirtualny spacer', max_length=100, required=False)
    development_plan = forms.URLField(label='Plan zagospodarowania', max_length=100, required=False)
    city = forms.CharField(label='Miejscowość', max_length=25)
    district = forms.CharField(label='Dzielnica', max_length=25)
    street = forms.CharField(label='ulica', max_length=50)
    description = forms.CharField(label='Opis', max_length=10000)
    type_of_building = forms.ChoiceField(choices=TypeOfBuilding.choices, label='Rodzaj zabudowy')
    floor = forms.IntegerField(label='Piętro')
    number_of_floors = forms.IntegerField(label='Liczba pięter')
    building_material = forms.ChoiceField(choices=BuildingMaterial.choices, label='Materiał budynku')
    windows = forms.ChoiceField(choices=Windows.choices, label='Okna')
    heating = forms.ChoiceField(choices=Heating.choices, label='Ogrzewanie')
    year_of_construction = forms.IntegerField(label='Rok budowy')
    finish_condition = forms.ChoiceField(choices=FinishCondition.choices, label='Stan wykończenia')
    rent = forms.IntegerField(label='Czynsz')
    currency_for_rent = forms.ChoiceField(choices=Currency.choices, label='')
    form_of_the_property = forms.ChoiceField(choices=FormOfTheProperty.choices, label='Forma własności')
    available_since = forms.DateTimeField(label='Dostępne od')
    internet = forms.BooleanField(required=False)
    cable_tv = forms.BooleanField(label='telewizja kablowa', required=False)
    phone = forms.BooleanField(label='telefon', required=False)
    anti_burglary_blinds = forms.BooleanField(label='rolety antywłamaniowe', required=False)
    security_doors_windows = forms.BooleanField(label='drzwi / okna antywłamaniowe', required=False)
    intercom_videophone = forms.BooleanField(label='domofon / wideofon', required=False)
    monitoring_security = forms.BooleanField(label='monitoring / ochrona', required=False)
    alarm_system = forms.BooleanField(label='system alarmowy', required=False)
    closed_area = forms.BooleanField(label='teren zamknięty', required=False)
    furniture = forms.BooleanField(label='meble', required=False)
    washing_machine = forms.BooleanField(label='pralka', required=False)
    dishwasher = forms.BooleanField(label='zmywarka', required=False)
    fridge = forms.BooleanField(label='lodówka', required=False)
    stove = forms.BooleanField(label='kuchenka', required=False)
    oven = forms.BooleanField(label='piekarnik', required=False)
    tv = forms.BooleanField(label='telewizor', required=False)
    balcony = forms.BooleanField(label='balkon', required=False)
    utility_room = forms.BooleanField(label='pom. użytkowe', required=False)
    garage_parking_space = forms.BooleanField(label='garaż/miejsce parkingowe', required=False)
    cellar = forms.BooleanField(label='piwnica', required=False)
    garden = forms.BooleanField(label='ogród', required=False)
    terrace = forms.BooleanField(label='taras', required=False)
    elevator = forms.BooleanField(label='winda', required=False)
    two_level = forms.BooleanField(label='dwupoziomowe', required=False)
    separate_kitchen = forms.BooleanField(label='oddzielna kuchnia', required=False)
    air_conditioning = forms.BooleanField(label='klimatyzacja', required=False)


class AdModelForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ["username",
                  "password",
                  "first_name",
                  "last_name",
                  "email",
                  "license_number",
                  "localization",
                  "telephone",
                  ]
