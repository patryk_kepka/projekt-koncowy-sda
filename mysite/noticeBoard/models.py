from django.db import models
from django.contrib.auth.models import User
from noticeBoard.Enums import (Currency,
                               RoomsNumber,
                               Market,
                               TypeOfBuilding,
                               BuildingMaterial,
                               Windows,
                               Heating,
                               FinishCondition,
                               FormOfTheProperty
                               )


class UserProfile(User):
    username = User.username
    first_name = User.first_name
    last_name = User.last_name
    email = User.email
    password = User.password
    license_number = models.IntegerField()
    localization = models.CharField(max_length=30)
    telephone = models.IntegerField()

    class Meta:
        verbose_name = "User Profile"


class AdModel(models.Model):
    title = models.CharField(max_length=50)
    price = models.DecimalField(decimal_places=2, max_digits=50)
    currency_for_price = models.CharField(
        max_length=5,
        choices=Currency.choices,
        default=Currency.PLN
    )
    area = models.DecimalField(decimal_places=2, max_digits=50)
    rooms_number = models.CharField(
        max_length=25,
        choices=RoomsNumber.choices
    )
    market = models.CharField(
        max_length=25,
        choices=Market.choices
    )
    movie = models.URLField(max_length=100)
    virtual_walk = models.URLField(max_length=100)
    development_plan = models.URLField(max_length=100)
    city = models.CharField(max_length=25)
    district = models.CharField(max_length=25, null=True, blank=True)
    street = models.CharField(max_length=50)
    description = models.CharField(max_length=10000)
    type_of_building = models.CharField(
        max_length=25,
        choices=TypeOfBuilding.choices
    )
    floor = models.IntegerField(default=0)
    number_of_floors = models.IntegerField(default=1)
    building_material = models.CharField(
        max_length=25,
        choices=BuildingMaterial.choices
    )
    windows = models.CharField(
        max_length=25,
        choices=Windows.choices
    )
    heating = models.CharField(
        max_length=25,
        choices=Heating.choices
    )
    year_of_construction = models.IntegerField()
    finish_condition = models.CharField(
        max_length=25,
        choices=FinishCondition.choices
    )
    rent = models.IntegerField()
    currency_for_rent = models.CharField(
        max_length=5,
        choices=Currency.choices,
        default=Currency.PLN
    )
    form_of_the_property = models.CharField(
        max_length=50,
        choices=FormOfTheProperty.choices
    )
    available_since = models.DateTimeField()
    internet = models.BooleanField()
    cable_tv = models.BooleanField()
    phone = models.BooleanField()
    anti_burglary_blinds = models.BooleanField()
    security_doors_windows = models.BooleanField()
    intercom_videophone = models.BooleanField()
    monitoring_security = models.BooleanField()
    alarm_system = models.BooleanField()
    closed_area = models.BooleanField()
    washing_machine = models.BooleanField()
    dishwasher = models.BooleanField()
    fridge = models.BooleanField()
    stove = models.BooleanField()
    oven = models.BooleanField()
    tv = models.BooleanField()
    balcony = models.BooleanField()
    utility_room = models.BooleanField()
    garage_parking_space = models.BooleanField()
    cellar = models.BooleanField()
    garden = models.BooleanField()
    terrace = models.BooleanField()
    elevator = models.BooleanField()
    two_level = models.BooleanField()
    separate_kitchen = models.BooleanField()
    air_conditioning = models.BooleanField()
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, related_name='advertising')

    # na samym końcu przycisk z podglądem (czy to nie funkcjonalność z html?) - to w templatce

    @property
    def price_per_squer_meter(self):
        return self.price / self.area


class AddImageModel(models.Model):
    image = models.ImageField()
    ad = models.ForeignKey(AdModel, on_delete=models.CASCADE, null=True, related_name='images')
