from django.urls import path
from . import views
from noticeBoard.views import AdView, SearchAdFormView, AdListView, AdDetailView
from noticeBoard.views import good_bye, good_job, good_new_user, NewUser

urlpatterns = [
    path('advertising/', AdView.as_view(), name='my-advertising'),
    path('advertising-form-view/', SearchAdFormView.as_view(), name='advertising-form-view'),
    path('advertising-list-view/', AdListView.as_view(), name='advertising-list-view'),
    path('advertising-detail-view/<pk>', AdDetailView.as_view(), name='advertising-detail-view'),
    path('new-user/', NewUser.as_view(), name="new_user"),
    path('good-bye/', good_bye, name='good_bye'),
    path('good-job/', good_job, name='good_job'),
    path('good-new-user/', good_new_user, name='good_new_user'),
]
